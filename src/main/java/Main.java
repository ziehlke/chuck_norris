import chuck.AChuckJoke;
import chuck.ParseUrlToJson;
import com.google.gson.Gson;

public class Main {
    private static final String LINK = "https://api.chucknorris.io/jokes/random";

    public static void main(String[] args) throws Exception {
        String json;

        for (int i = 0; i <5; i++) {
            json = ParseUrlToJson.readUrl(LINK);
            Gson gson = new Gson();
            AChuckJoke response = gson.fromJson(json, AChuckJoke.class);
            System.out.println(response.joke);

        }
    }
}
